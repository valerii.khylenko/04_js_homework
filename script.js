let userNumA = +prompt('Enter the first number');
while (Number.isNaN(userNumA)) {
    userNumA = +prompt("Number again please");
}

let userNumB = +prompt("Enter the second number");
while (Number.isNaN(userNumB)) {
    userNumB = +prompt('Number again please');
}

let selectFunc;
do {
    selectFunc = prompt(`Select math action: "+", "-" , "*" , "/"`);
} while (!(selectFunc === "+" || selectFunc === "-" || selectFunc === "*" || selectFunc === "/"));


function runFunc(userNumA, userNumB, selectFunc) {
    if (selectFunc === "+") {
        return userNumA + userNumB;
    }
    if (selectFunc === "-") {
        return userNumA - userNumB;
    }
    if (selectFunc === "*") {
        return userNumA * userNumB;
    }
    if (selectFunc === "/") {
        return userNumA / userNumB;
    }
}
console.log(runFunc(userNumA, userNumB, selectFunc));